var toHTML = require('vdom-to-html');
var assert = require('assert');

var render = require('../../components/breadcrumbs.js');
var data = require('../fixture/breadcrumbs_view.js');

suite('Component Render', function () {

  test('Render w/ links', function () {
    var dom;

    try {
      dom = render(data);
    } catch (e) {
      assert.ifError(e);
    }

    console.log(toHTML(dom));
  });

  test('Render - Null data', function () {
    var dom;

    try {
      dom = render(null);
    } catch (e) {
      assert.ifError(e);
    }

    console.log(toHTML(dom));
  });

});

var httpContext = {
  controller: {
    name: 'breadcrumbs_view'
  }
};

module.exports = {
  view_model: {},
  httpContext: httpContext,
  page_model: {
    title: 'CBANC Breadcrumbs',
    stylesheets: [
      './index.css'
    ],
    breadcrumbs: [{
      text: 'Home',
      className: "dogs",
      href: '/'
    }, {
      text: 'Documents',
      href: '#'
    }, {
      text: 'Document Title Goes Here',
      href: '#'
    }],
    header: false,
    footer: false
  }
};

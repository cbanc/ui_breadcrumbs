var assert = require('assert');

module.exports = function (data, page_render) {
  var dom;
  var render = require('ui_page')(page_render);

  try {
    dom = render(data);
  } catch (e) {
    assert.ifError(e);
  }
  return dom;
};

#ui_breadcrumbs

CBANC breadcrumbs component. This component is packaged ui_page. Unless you are specifically working on the breadcrumbs, you should never have to directly include this.

# Install

*Page Usage* - Install ui_page and build something.

```
#!js
"dependencies": {
  "ui_page": "git+ssh://git@bitbucket.org:cbanc/ui_page.git"
}
```

*Direct Usage* - Add the git url to package.json. If you use it this way, then you will need to scaffold the page around it.

```
#!js
"dependencies": {
  "ui_breadcrumbs": "git+ssh://git@bitbucket.org:cbanc/ui_breadcrumbs.git"
}
```

#Mixins

Mixin for breadcrumbs should be used as:

```
#!scss
#breadcrumbs {
  @include breadcrumbs();
}
```

# Configuration

The breadcrumbs items depend on a configuration block in the page_model.

```
#!js
page_model: {
  title: 'CBANC Title',
  stylesheets: [
    './index.css',
    'http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'
  ],
  breadcrumbs: [{
    text: 'Home',
    href: '/'
  }, {
    text: 'Documents',
    href: 'documents'
  }, {
    text: 'Some Document Title'
  }]
}
```
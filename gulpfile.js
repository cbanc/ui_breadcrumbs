var gulp = require('gulp');

var options = {
  page_table: {
  	main: null
  }
};

var ui_gulp = require('ui_gulp')(options);

gulp.task('default', ui_gulp.default);

gulp.task('watch', ui_gulp.watch);
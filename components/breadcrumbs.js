var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');

module.exports = versionify(function (state) {
  state = state || {};
  var page_model = state.page_model || {};
  var breadcrumbs = page_model.breadcrumbs || [];

  if (breadcrumbs && breadcrumbs.length) {
    return h("div#breadcrumbs",
      h("ul", {
          attributes: {
            itemscope: "",
            itemtype: "http://schema.org/BreadcrumbList"
          }
        },
        breadcrumbs.map(function (t, i) {
          var item;
          var span_className = t.className;

          if (i < breadcrumbs.length - 1) {
            item = anchor({
              text: h("span", {
                className: span_className,
                attributes: {
                  itemprop: "title"
                }
              }, t.text),
              href: t.href,
              attributes: {
                itemprop: "url"
              }
            });
          } else {
            item = h("span", {
              className: span_className,
              attributes: {
                itemprop: "title"
              }
            }, t.text);
          }

          return h("li", {
            attributes: {
              itemscope: "",
              itemtype: "http://schema.org/ListItem"
            }
          }, item);
        }))
    );
  }

  return null;
});
